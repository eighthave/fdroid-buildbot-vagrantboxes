Vagrant.configure("2") do |config|
  config.vm.box = "debian/bookworm64"
  config.vm.provider :libvirt do |libvirt|
    libvirt.driver = "kvm"
    libvirt.host = "localhost"
    libvirt.uri = "qemu:///system"
    libvirt.cpus = 2
    libvirt.memory = 512
    libvirt.management_network_address = "192.168.200.0/24"
    libvirt.management_network_name = "fdroid-buildbot-master"
  end

  config.vm.network "forwarded_port", guest: 8010, host: 8010, host_ip: "127.0.0.1"

  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provision "shell", inline: <<-SHELL
    export DEBIAN_FRONTEND=noninteractive
    export LANG=C.UTF-8
    echo Etc/UTC > /etc/timezone
    echo 'APT::Install-Recommends "0";' \
         'APT::Install-Suggests "0";' \
         'APT::Get::Assume-Yes "true";' \
         'Acquire::Retries "20";' \
         'Dpkg::Use-Pty "0";' \
         'quiet "1";' \
        >> /etc/apt/apt.conf.d/99gitlab

    echo master > /etc/hostname
    sed -i 's,localhost$,localhost master,' /etc/hosts

    apt-get update
    apt-get -qy upgrade
    apt-get -qy dist-upgrade
    apt-get -qy autoremove
    apt-get -qy autoclean
    apt-get install -y buildbot buildbot-worker git python3-pip

    buildbot_version=$(dpkg-query --showformat='${Version}' --show buildbot)
    pip install \
        "buildbot-console-view==${buildbot_version}" \
        "buildbot-grid-view==${buildbot_version}" \
        "buildbot-waterfall-view==${buildbot_version}" \
        "buildbot-www==${buildbot_version}" \

    sudo -u vagrant test -e master/master.cfg || \
        (buildbot create-master master && mv master/master.cfg.sample master/master.cfg)
    sudo -u vagrant buildbot start master
    sudo -u vagrant buildbot-worker create-worker worker localhost example-worker pass
    sudo -u vagrant buildbot-worker start worker

  SHELL
end
